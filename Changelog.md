## Autor:Andrea Calleja
## ChangeLog
## v3.0 2020-12-01

### Added
> bot.cpp controlará la raqueta del jugador 1 y si el jugador 2 no ha pulsado ninguna tecla en 10 segundos, también podrá controlar la raqueta del jugador 2.
> logger.cpp un ejecutable que se comunicará con Mundo a través de FIFOtenis.
> DatosMemCompartida.h necesaria para la comunicación entre bot y Mundo.

### Modificated
> CMakeLists.txt para implementar el .cpp de bot y logger.
> Mundo.cpp y .h para establecer la comunicacion con los nuevos ejecutables así como para la finalización del programa cuando alguno de los dos jugadores llegue a 3 puntos.

## v2.1 2020-10-30

### Added
> Funcion disminuye para que la pelota vaya disminuyendo su radio a medida que aumenta el tiempo
> Readme.txt que contiene las reglas del juego

### Modificated
> Clase esfera y raqueta para posibilitar el movimiento


## v1.2 2020-10-13

Fin de la practica 1

## v1.1 2020-10-13

### Added
Cambio de cabecera





